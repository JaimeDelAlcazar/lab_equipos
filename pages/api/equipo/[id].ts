import { NextApiRequest, NextApiResponse } from "next";
import DB from '@database';


const allEquipos = async ( request: NextApiRequest ,response: NextApiResponse) => {
    const db = new DB()

    const id = request.query.id

    const equipo = await db.getById(id as string)

    response.status(200).json(equipo)
}   

export default allEquipos