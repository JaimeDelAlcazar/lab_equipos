import React, { useState, useEffect } from 'react';
import Navbar from '../components/Navbar/Navbar';
import Button from 'react-bootstrap/Button';

const HomePage = () => {
    const [equipoList, setEquipoList] = useState([])

    // const socketOptions = {cors: {origin: '*',credentials: true,}}
    
    useEffect(() => {
        window
        .fetch('/api/equipo')
        .then(response => response.json())
        .then(({ data, length }) => {
            setEquipoList(data)
            console.log(data)
            console.log(equipoList)
        })
    },[])

    return (
        <div>
            {/* <Button variant="danger">Primary</Button>{' '} */}
            {/* <p>Hola mundo</p>
            {equipoList.map((equipo) => {
                <div>{equipo.name}</div>
            })} */}
            {/* <img src="" alt="" srcset=""/> */}
            <div className="intro">
                <p className="intro__title">CAPACIDADES CITA</p>
                <p className="intro__text">RESERVA DE EQUIPOS E INSTRUMENTACIÓN</p>
            </div>


            <div className="step_container"><p className="step_text">PASO 1: Revisa qué equipos tenemos en el CITA.</p>
            </div>
            <div className="main_container">
                <div className="element_"></div>
            </div>
            <div className="step_container"><p className="step_text">PASO 2: Revisa la disponibilidad en el calendario.</p>
            </div>
            <div className="main_container">
                <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FLima&amp;src=ZXMucGUjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%230B8043" style={{border:"solid 1px #777" }} width="800" height="600" scrolling="no"></iframe>
                {/* <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FLima&amp;src=ZXMucGUjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%230B8043" style={{border:"solid 1px #777" }} width="800" height="600" frameborder="0" scrolling="no"></iframe> */}
                {/* <div className="element_"></div> */}
            </div>
            <div className="step_container"><p className="step_text">PASO 3: Luego de verificar la disponibilidad, procede con la solicitud a través del formulario.</p>
            </div>  
            <div className="main_container">
                {/* <div className="element_"></div> */}
                <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfPZMordiJHC-0semlIkjjkJIW775FVtaLXjOFddbzpJaaFmA/viewform?embedded=true" width="640" height="1436" >Cargando…</iframe>
                {/* <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfPZMordiJHC-0semlIkjjkJIW775FVtaLXjOFddbzpJaaFmA/viewform?embedded=true" width="640" height="1436" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe> */}
                {/* <iframe src="https://docs.google.com/forms/d/e//* 1FAIpQLSfPZMordiJHC-0semlIkjjkJIW775FVtaLXjOFddbzpJaaFmA/viewform?embedded=true" width="640" height="1436" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe> */}
            </div>
            <div className="step_container"><p className="step_text">PASO 4: Recibirás las constancia de tu solicitud en tu correo y en los próximos días, la respuesta del Lab Manager con la aprobación del PI. Recuerda que, mínimamente, deberás hacer tu reserva/pedido con dos semanas de anticipación.</p>
            </div>
        </div>
    )
}

export default HomePage
