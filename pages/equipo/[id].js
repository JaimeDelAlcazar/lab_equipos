import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import fetch from 'isomorphic-unfetch'

const EquipoItem = () => {
    const { query } = useRouter();
    const [product, setProduct] = useState([])

    useEffect(() => {
    if (query.id) {
      fetch(`/api/equipo/${query.id}`)
        .then((response) => response.json())
        .then((data) => {
          setProduct(data)
          console.log(product)
          console.log(data)
        })
    }
  }, [query.id])

  return (
      <div>
          <p>Equipo</p>
          {/* <p>Equipo `${product.name}`</p> */}
          <p>Equipo {product.name}</p>
          {/* {product => {
                <div>{product.name}</div>
            }} */}
            {/* {product.map((equipo) => {
                <div>{equipo.name}</div>
            })} */}
        {/* {product == null ? null : <ProductSummary product={product} />} */}
      </div>
  )
}

export default EquipoItem
