import React from 'react';
import { useRouter } from 'next/router'

const EquipoItem = () => {
    const { query: { id }} = useRouter();
    return (
        <div>
            Página del equipo: {id}
        </div>
    )
}

export default EquipoItem
