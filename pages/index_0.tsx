import React, { useState, useEffect } from 'react';
import Navbar from '../components/Navbar/Navbar';

const HomePage = () => {
    const [equipoList, setEquipoList] = useState([])

    // const socketOptions = {cors: {origin: '*',credentials: true,}}
    
    useEffect(() => {
        window
        .fetch('/api/equipo')
        .then(response => response.json())
        .then(({ data, length }) => {
            setEquipoList(data)
            console.log(data)
            console.log(equipoList)
        })
    },[])

    return (
        <div>
            <p>Hola mundo</p>
            {equipoList.map((equipo) => {
                <div>{equipo.name}</div>
            })}
        </div>
    )
}

export default HomePage
