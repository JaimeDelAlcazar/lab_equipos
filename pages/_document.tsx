import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head />
        {/* <Head>
          <title>Capacidades CITA</title>
        </Head> */}
        <body className="body-class">
          <Main />

          <NextScript />

          
          {/* <NextScript>
            <script src="https://unpkg.com/react/umd/react.production.min.js"></script>

            <script
            src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"></script>

            <script
            src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"></script>
            <script>var Alert = ReactBootstrap.Alert;</script>
          </NextScript>   */}



          {/* <NextScript>
          <script src="https://unpkg.com/react/umd/react.production.min.js" crossorigin></script>

        <script
        src="https://unpkg.com/react-dom/umd/react-dom.production.min.js" crossorigin></script>

        <script
        src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"  crossorigin></script>
          </NextScript>   */}  
        </body>
      </Html>
    )
  }
}

export default MyDocument