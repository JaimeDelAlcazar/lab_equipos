webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.tsx":
/*!*************************!*\
  !*** ./pages/index.tsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


var _jsxFileName = "C:\\Users\\JAIME\\Desktop\\UTEC\\WEB-RESERVA-EQUIPOS\\capacidades_cita\\pages\\index.tsx",
    _this = undefined,
    _s = $RefreshSig$();



var HomePage = function HomePage() {
  _s();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      equipoList = _useState[0],
      setEquipoList = _useState[1]; // const socketOptions = {cors: {origin: '*',credentials: true,}}


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    window.fetch('/api/equipo').then(function (response) {
      return response.json();
    }).then(function (_ref) {
      var data = _ref.data,
          length = _ref.length;
      setEquipoList(data);
      console.log(data);
      console.log(equipoList);
    });
  }, []);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "intro",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
        className: "intro__title",
        children: "CAPACIDADES CITA"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 17
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
        className: "intro__text",
        children: "RESERVA DE EQUIPOS E INSTRUMENTACI\xD3N"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 17
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "step_container",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
        className: "step_text",
        children: "PASO 1: Revisa qu\xE9 equipos tenemos en el CITA."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 45
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "main_container",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "element_"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 17
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "step_container",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
        className: "step_text",
        children: "PASO 2: Revisa la disponibilidad en el calendario."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 45
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "main_container",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("iframe", {
        src: "https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=America%2FLima&src=ZXMucGUjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&color=%230B8043",
        style: {
          border: "solid 1px #777"
        },
        width: "800",
        height: "600",
        scrolling: "no"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 17
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "step_container",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
        className: "step_text",
        children: "PASO 3: Luego de verificar la disponibilidad, procede con la solicitud a trav\xE9s del formulario."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 45
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "main_container",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("iframe", {
        src: "https://docs.google.com/forms/d/e/1FAIpQLSfPZMordiJHC-0semlIkjjkJIW775FVtaLXjOFddbzpJaaFmA/viewform?embedded=true",
        width: "640",
        height: "1436",
        frameborder: "0",
        marginheight: "0",
        marginwidth: "0",
        children: "Cargando\u2026"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 51,
        columnNumber: 17
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 13
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "step_container",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
        className: "step_text",
        children: "PASO 4: Recibir\xE1s las constancia de tu solicitud en tu correo y en los pr\xF3ximos d\xEDas, la respuesta del Lab Manager con la aprobaci\xF3n del PI. Recuerda que, m\xEDnimamente, deber\xE1s hacer tu reserva/pedido con dos semanas de anticipaci\xF3n."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 45
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 13
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 22,
    columnNumber: 9
  }, _this);
};

_s(HomePage, "R5a13Rntm7aoGvZSiDOQr4Agy8c=");

_c = HomePage;
/* harmony default export */ __webpack_exports__["default"] = (HomePage);

var _c;

$RefreshReg$(_c, "HomePage");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXgudHN4Il0sIm5hbWVzIjpbIkhvbWVQYWdlIiwidXNlU3RhdGUiLCJlcXVpcG9MaXN0Iiwic2V0RXF1aXBvTGlzdCIsInVzZUVmZmVjdCIsIndpbmRvdyIsImZldGNoIiwidGhlbiIsInJlc3BvbnNlIiwianNvbiIsImRhdGEiLCJsZW5ndGgiLCJjb25zb2xlIiwibG9nIiwiYm9yZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7QUFJQSxJQUFNQSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxHQUFNO0FBQUE7O0FBQUEsa0JBQ2lCQyxzREFBUSxDQUFDLEVBQUQsQ0FEekI7QUFBQSxNQUNaQyxVQURZO0FBQUEsTUFDQUMsYUFEQSxpQkFHbkI7OztBQUVBQyx5REFBUyxDQUFDLFlBQU07QUFDWkMsVUFBTSxDQUNMQyxLQURELENBQ08sYUFEUCxFQUVDQyxJQUZELENBRU0sVUFBQUMsUUFBUTtBQUFBLGFBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxFQUFKO0FBQUEsS0FGZCxFQUdDRixJQUhELENBR00sZ0JBQXNCO0FBQUEsVUFBbkJHLElBQW1CLFFBQW5CQSxJQUFtQjtBQUFBLFVBQWJDLE1BQWEsUUFBYkEsTUFBYTtBQUN4QlIsbUJBQWEsQ0FBQ08sSUFBRCxDQUFiO0FBQ0FFLGFBQU8sQ0FBQ0MsR0FBUixDQUFZSCxJQUFaO0FBQ0FFLGFBQU8sQ0FBQ0MsR0FBUixDQUFZWCxVQUFaO0FBQ0gsS0FQRDtBQVFILEdBVFEsRUFTUCxFQVRPLENBQVQ7QUFXQSxzQkFDSTtBQUFBLDRCQU9JO0FBQUssZUFBUyxFQUFDLE9BQWY7QUFBQSw4QkFDSTtBQUFHLGlCQUFTLEVBQUMsY0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURKLGVBRUk7QUFBRyxpQkFBUyxFQUFDLGFBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFQSixlQWFJO0FBQUssZUFBUyxFQUFDLGdCQUFmO0FBQUEsNkJBQWdDO0FBQUcsaUJBQVMsRUFBQyxXQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFiSixlQWVJO0FBQUssZUFBUyxFQUFDLGdCQUFmO0FBQUEsNkJBQ0k7QUFBSyxpQkFBUyxFQUFDO0FBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFmSixlQWtCSTtBQUFLLGVBQVMsRUFBQyxnQkFBZjtBQUFBLDZCQUFnQztBQUFHLGlCQUFTLEVBQUMsV0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBLGFBbEJKLGVBb0JJO0FBQUssZUFBUyxFQUFDLGdCQUFmO0FBQUEsNkJBQ0k7QUFBUSxXQUFHLEVBQUMsK0tBQVo7QUFBZ04sYUFBSyxFQUFFO0FBQUNZLGdCQUFNLEVBQUM7QUFBUixTQUF2TjtBQUFtUCxhQUFLLEVBQUMsS0FBelA7QUFBK1AsY0FBTSxFQUFDLEtBQXRRO0FBQTRRLGlCQUFTLEVBQUM7QUFBdFI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFwQkosZUF5Qkk7QUFBSyxlQUFTLEVBQUMsZ0JBQWY7QUFBQSw2QkFBZ0M7QUFBRyxpQkFBUyxFQUFDLFdBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQXpCSixlQTJCSTtBQUFLLGVBQVMsRUFBQyxnQkFBZjtBQUFBLDZCQUVJO0FBQVEsV0FBRyxFQUFDLG1IQUFaO0FBQWdJLGFBQUssRUFBQyxLQUF0STtBQUE0SSxjQUFNLEVBQUMsTUFBbko7QUFBMEosbUJBQVcsRUFBQyxHQUF0SztBQUEwSyxvQkFBWSxFQUFDLEdBQXZMO0FBQTJMLG1CQUFXLEVBQUMsR0FBdk07QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBM0JKLGVBZ0NJO0FBQUssZUFBUyxFQUFDLGdCQUFmO0FBQUEsNkJBQWdDO0FBQUcsaUJBQVMsRUFBQyxXQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFoQ0o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBREo7QUFxQ0gsQ0FyREQ7O0dBQU1kLFE7O0tBQUFBLFE7QUF1RFNBLHVFQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LmUwMjFlZjY0Yjg3NWRhYTNjYzU0LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IE5hdmJhciBmcm9tICcuLi9jb21wb25lbnRzL05hdmJhci9OYXZiYXInO1xyXG5pbXBvcnQgQnV0dG9uIGZyb20gJ3JlYWN0LWJvb3RzdHJhcC9CdXR0b24nO1xyXG5cclxuY29uc3QgSG9tZVBhZ2UgPSAoKSA9PiB7XHJcbiAgICBjb25zdCBbZXF1aXBvTGlzdCwgc2V0RXF1aXBvTGlzdF0gPSB1c2VTdGF0ZShbXSlcclxuXHJcbiAgICAvLyBjb25zdCBzb2NrZXRPcHRpb25zID0ge2NvcnM6IHtvcmlnaW46ICcqJyxjcmVkZW50aWFsczogdHJ1ZSx9fVxyXG4gICAgXHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgICAgIHdpbmRvd1xyXG4gICAgICAgIC5mZXRjaCgnL2FwaS9lcXVpcG8nKVxyXG4gICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHJlc3BvbnNlLmpzb24oKSlcclxuICAgICAgICAudGhlbigoeyBkYXRhLCBsZW5ndGggfSkgPT4ge1xyXG4gICAgICAgICAgICBzZXRFcXVpcG9MaXN0KGRhdGEpXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVxdWlwb0xpc3QpXHJcbiAgICAgICAgfSlcclxuICAgIH0sW10pXHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICB7LyogPEJ1dHRvbiB2YXJpYW50PVwiZGFuZ2VyXCI+UHJpbWFyeTwvQnV0dG9uPnsnICd9ICovfVxyXG4gICAgICAgICAgICB7LyogPHA+SG9sYSBtdW5kbzwvcD5cclxuICAgICAgICAgICAge2VxdWlwb0xpc3QubWFwKChlcXVpcG8pID0+IHtcclxuICAgICAgICAgICAgICAgIDxkaXY+e2VxdWlwby5uYW1lfTwvZGl2PlxyXG4gICAgICAgICAgICB9KX0gKi99XHJcbiAgICAgICAgICAgIHsvKiA8aW1nIHNyYz1cIlwiIGFsdD1cIlwiIHNyY3NldD1cIlwiLz4gKi99XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW50cm9cIj5cclxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImludHJvX190aXRsZVwiPkNBUEFDSURBREVTIENJVEE8L3A+XHJcbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJpbnRyb19fdGV4dFwiPlJFU0VSVkEgREUgRVFVSVBPUyBFIElOU1RSVU1FTlRBQ0nDk048L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3RlcF9jb250YWluZXJcIj48cCBjbGFzc05hbWU9XCJzdGVwX3RleHRcIj5QQVNPIDE6IFJldmlzYSBxdcOpIGVxdWlwb3MgdGVuZW1vcyBlbiBlbCBDSVRBLjwvcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWFpbl9jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZWxlbWVudF9cIj48L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3RlcF9jb250YWluZXJcIj48cCBjbGFzc05hbWU9XCJzdGVwX3RleHRcIj5QQVNPIDI6IFJldmlzYSBsYSBkaXNwb25pYmlsaWRhZCBlbiBlbCBjYWxlbmRhcmlvLjwvcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWFpbl9jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgIDxpZnJhbWUgc3JjPVwiaHR0cHM6Ly9jYWxlbmRhci5nb29nbGUuY29tL2NhbGVuZGFyL2VtYmVkP2hlaWdodD02MDAmYW1wO3drc3Q9MSZhbXA7Ymdjb2xvcj0lMjNmZmZmZmYmYW1wO2N0ej1BbWVyaWNhJTJGTGltYSZhbXA7c3JjPVpYTXVjR1VqYUc5c2FXUmhlVUJuY205MWNDNTJMbU5oYkdWdVpHRnlMbWR2YjJkc1pTNWpiMjAmYW1wO2NvbG9yPSUyMzBCODA0M1wiIHN0eWxlPXt7Ym9yZGVyOlwic29saWQgMXB4ICM3NzdcIiB9fSB3aWR0aD1cIjgwMFwiIGhlaWdodD1cIjYwMFwiIHNjcm9sbGluZz1cIm5vXCI+PC9pZnJhbWU+XHJcbiAgICAgICAgICAgICAgICB7LyogPGlmcmFtZSBzcmM9XCJodHRwczovL2NhbGVuZGFyLmdvb2dsZS5jb20vY2FsZW5kYXIvZW1iZWQ/aGVpZ2h0PTYwMCZhbXA7d2tzdD0xJmFtcDtiZ2NvbG9yPSUyM2ZmZmZmZiZhbXA7Y3R6PUFtZXJpY2ElMkZMaW1hJmFtcDtzcmM9WlhNdWNHVWphRzlzYVdSaGVVQm5jbTkxY0M1MkxtTmhiR1Z1WkdGeUxtZHZiMmRzWlM1amIyMCZhbXA7Y29sb3I9JTIzMEI4MDQzXCIgc3R5bGU9e3tib3JkZXI6XCJzb2xpZCAxcHggIzc3N1wiIH19IHdpZHRoPVwiODAwXCIgaGVpZ2h0PVwiNjAwXCIgZnJhbWVib3JkZXI9XCIwXCIgc2Nyb2xsaW5nPVwibm9cIj48L2lmcmFtZT4gKi99XHJcbiAgICAgICAgICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9XCJlbGVtZW50X1wiPjwvZGl2PiAqL31cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3RlcF9jb250YWluZXJcIj48cCBjbGFzc05hbWU9XCJzdGVwX3RleHRcIj5QQVNPIDM6IEx1ZWdvIGRlIHZlcmlmaWNhciBsYSBkaXNwb25pYmlsaWRhZCwgcHJvY2VkZSBjb24gbGEgc29saWNpdHVkIGEgdHJhdsOpcyBkZWwgZm9ybXVsYXJpby48L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PiAgXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWFpbl9jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT1cImVsZW1lbnRfXCI+PC9kaXY+ICovfVxyXG4gICAgICAgICAgICAgICAgPGlmcmFtZSBzcmM9XCJodHRwczovL2RvY3MuZ29vZ2xlLmNvbS9mb3Jtcy9kL2UvMUZBSXBRTFNmUFpNb3JkaUpIQy0wc2VtbElramprSklXNzc1RlZ0YUxYak9GZGRienBKYWFGbUEvdmlld2Zvcm0/ZW1iZWRkZWQ9dHJ1ZVwiIHdpZHRoPVwiNjQwXCIgaGVpZ2h0PVwiMTQzNlwiIGZyYW1lYm9yZGVyPVwiMFwiIG1hcmdpbmhlaWdodD1cIjBcIiBtYXJnaW53aWR0aD1cIjBcIj5DYXJnYW5kb+KApjwvaWZyYW1lPlxyXG4gICAgICAgICAgICAgICAgey8qIDxpZnJhbWUgc3JjPVwiaHR0cHM6Ly9kb2NzLmdvb2dsZS5jb20vZm9ybXMvZC9lLy8qIDFGQUlwUUxTZlBaTW9yZGlKSEMtMHNlbWxJa2pqa0pJVzc3NUZWdGFMWGpPRmRkYnpwSmFhRm1BL3ZpZXdmb3JtP2VtYmVkZGVkPXRydWVcIiB3aWR0aD1cIjY0MFwiIGhlaWdodD1cIjE0MzZcIiBmcmFtZWJvcmRlcj1cIjBcIiBtYXJnaW5oZWlnaHQ9XCIwXCIgbWFyZ2lud2lkdGg9XCIwXCI+Q2FyZ2FuZG/igKY8L2lmcmFtZT4gKi99XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInN0ZXBfY29udGFpbmVyXCI+PHAgY2xhc3NOYW1lPVwic3RlcF90ZXh0XCI+UEFTTyA0OiBSZWNpYmlyw6FzIGxhcyBjb25zdGFuY2lhIGRlIHR1IHNvbGljaXR1ZCBlbiB0dSBjb3JyZW8geSBlbiBsb3MgcHLDs3hpbW9zIGTDrWFzLCBsYSByZXNwdWVzdGEgZGVsIExhYiBNYW5hZ2VyIGNvbiBsYSBhcHJvYmFjacOzbiBkZWwgUEkuIFJlY3VlcmRhIHF1ZSwgbcOtbmltYW1lbnRlLCBkZWJlcsOhcyBoYWNlciB0dSByZXNlcnZhL3BlZGlkbyBjb24gZG9zIHNlbWFuYXMgZGUgYW50aWNpcGFjacOzbi48L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBIb21lUGFnZVxyXG4iXSwic291cmNlUm9vdCI6IiJ9