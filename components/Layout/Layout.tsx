import React from 'react'
import NavbarEquipos from '@components/Navbar/Navbar'

const Layout: React.FC = ({ children }) => {
    return (
        <div>
            <NavbarEquipos />
            {children}
            {/* <footer>Footer</footer> */}
        </div>
    )
}

export default Layout
